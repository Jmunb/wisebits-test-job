export type TagType = {
    text: string,
    color: string
}
