import { getRandomColor } from './utils'
import type { TagType } from './types/tag.type'
import type { CardType } from './types/card.type'

const api = 'https://random-data-api.com/api/coffee/random_coffee'
const tagsStore = localStorage.getItem('wisebits_coffee_tags')
const tags = new Map<string, string>(tagsStore ? JSON.parse(tagsStore) : null)

window.addEventListener('beforeunload', function () {
    localStorage.setItem('wisebits_coffee_tags', JSON.stringify(Array.from(tags.entries())))
})

function formatAndSaveTags (tag: string): TagType {
    const key = tag.trim().toLowerCase()
    let color: string

    if (!tags.has(key)) {
        color = getRandomColor()
        tags.set(key, color)
    } else {
        color = tags.get(key)
    }

    return { text: key, color }
}

export async function fetchCard (): Promise<CardType> {
    const response = await fetch(api)
    // if (!response.ok || !response.status === 200) {
    //     throw new Error('eee')
    // }
    const json = await response.json()
    // json.origin = json.origin.split(',').map((item: string) => item.trim())
    json.formatNotes = json.notes
        .split(',')
        .map(formatAndSaveTags)

    return json
}
